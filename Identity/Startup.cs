﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Identity
{
  public class Startup
  {
    public IWebHostEnvironment Environment { get; }

    public Startup(IWebHostEnvironment environment)
    {
      Environment = environment;
    }

    public void ConfigureServices(IServiceCollection services)
    {
      var builder = services.AddIdentityServer()
        .AddInMemoryIdentityResources(Config.Ids)
        .AddInMemoryApiResources(Config.Apis)
        .AddInMemoryClients(Config.Clients);

      if (Environment.IsDevelopment())
      {
        // not recommended for production - you need to store your key material somewhere secure
        builder.AddDeveloperSigningCredential();
      }
      else
      {
        // load the certificates as per production Identity app
      }
    }

    public void Configure(IApplicationBuilder app)
    {
      if (Environment.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseIdentityServer();
    }
  }
}
