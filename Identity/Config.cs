﻿using IdentityServer4.Models;
using System.Collections.Generic;

namespace Identity
{
  public static class Config
  {
    public static IEnumerable<IdentityResource> Ids =>
      new IdentityResource[]
      { 
        new IdentityResources.OpenId()
      };

    public static IEnumerable<ApiResource> Apis =>
      new ApiResource[]
      {
        new ApiResource("all:read", "Api")
      };

    public static IEnumerable<Client> Clients =>
      new List<Client>
      {
        new Client
        {
          ClientId = "HHAExchange.ENT.APP",

          // no interactive user, use the clientid/secret for authentication
          AllowedGrantTypes = GrantTypes.ClientCredentials,

          // secret for authentication
          ClientSecrets =
          {
            new Secret("Md@Z#9eS8EW!lkkd1YhL29mZg114enhxWo".Sha256())
          },

          // scopes that client has access to
          AllowedScopes = { "all:read" }
        }
      };
  }
}