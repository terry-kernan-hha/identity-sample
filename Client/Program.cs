﻿using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Client
{
  public class Program
  {
    private static async Task Main()
    {
      // discover endpoints from metadata
      var client = new HttpClient();

      var disco = await client.GetDiscoveryDocumentAsync("https://app.hhaexchange.com/identity");
      if (disco.IsError)
      {
        Console.WriteLine($"Discovery Error: {disco.Error}");
        return;
      }

      // request token
      var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
      {
        Address = disco.TokenEndpoint,
        ClientId = "HHAExchange.ENT.APP",
        ClientSecret = "Md@Z#9eS8EW!lkkd1YhL29mZg114enhxWo",
        Scope = "all:read"
      });

      if (tokenResponse.IsError)
      {
        Console.WriteLine($"Token Repsonse Error: {tokenResponse.Error}");
        return;
      }

      Console.WriteLine(tokenResponse.Json);
      Console.WriteLine("\n\n");

      var apiClient = new HttpClient();
      apiClient.SetBearerToken(tokenResponse.AccessToken);

      Console.WriteLine("request a protected resource: http://localhost:5001/claims");
      var response = await apiClient.GetAsync("http://localhost:5001/claims");
      if (!response.IsSuccessStatusCode)
      {
        Console.WriteLine(response.StatusCode);
      }
      else
      {
        var content = await response.Content.ReadAsStringAsync();
        Console.WriteLine(JArray.Parse(content));
      }

      Console.ReadKey();
    }
  }
}