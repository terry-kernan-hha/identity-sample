## Overview

This is a quick sample of how to use IdentityServer4 (not to be confused with the Identity application) to provide authentication for API requests.

It's important that the credentials are shared in a central place, as using secrets and keys from different sources will produce different results, as keys and certs won't match.

## What's Included

- Identity - only included as a quick sample, but for a proper implementation, use the Identity app at https://app.hhaechange.com/identity
- Api - Sample api with a protected endpoint that requires authorisation.  Includes automatic swagger documentation generation, with support for bearer authentication provided by Identity.
- Client - Sample console application that requests bearer token from Identity, then uses it to request data from the Api project.

## Running The Samples

this requires dotnet core 3.1!

simply run the bat files in Api first, then Client.  You should see the output that the Client produces.  To verify that the bearer token is used, simply browse to http://localhost:5001/claims and you should see that it returns a 401 reponse, requires authentication.

