﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers {

  [Route("claims")]
  [Authorize]
  public class ClaimsController : ControllerBase
  {
    /// <summary>
    /// Gets the claims for the currently logged in user
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public IActionResult Get()
    {
      return new JsonResult(from c in User.Claims select new {c.Type, c.Value});
    }
  }
}
